import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {NavComponent} from './nav/nav.component';
import {RouterModule} from '@angular/router';
import {AppRouterModule} from './app-router.module';
import {NavActionsComponent} from './nav/nav-actions/nav-actions.component';
import {FormsModule} from '@angular/forms';
import {NavSelectComponent} from './nav/nav-select/nav-select.component';
import {HomeComponent} from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    NavActionsComponent,
    NavSelectComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRouterModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
