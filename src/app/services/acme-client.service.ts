import {Injectable} from '@angular/core';
import {AcmeClient, AcmeSelectOption} from '../domain-defs';
import {Observable} from 'rxjs';
import {AcmeMockHttpService} from './acme-mock-http.service';

@Injectable({
  providedIn: 'root'
})
export class AcmeClientService {
  private readonly mockData: AcmeClient[];

  constructor(private readonly mockHttp: AcmeMockHttpService) {
    this.mockData = [
      {
        id: 1,
        firstName: 'John',
        lastName: 'Doe',
        address: {
          address1: '123 Easy St',
          address2: null,
          city: 'Atlanta',
          state_province: 'GA',
          postal_code: '30308',
          country: 'USA'
        }
      },
      {
        id: 2,
        firstName: 'Jane',
        lastName: 'Smith',
        address: {
          address1: '789 Last Ave',
          address2: 'Suite A',
          city: 'Ear Falls',
          state_province: 'ON',
          postal_code: 'POV 1T0',
          country: 'Canada'
        }
      }
    ];
  }

  findByClientId(id: number): Observable<AcmeClient> {
    return this.mockHttp.get(() => this.mockData.find(c => c.id === id));
  }

  getClientNames(): Observable<AcmeSelectOption[]> {
    const options = this.mockData.map(c => ({id: c.id, display: `${c.firstName} ${c.lastName}`}));
    return this.mockHttp.get(() => options);
  }
}
