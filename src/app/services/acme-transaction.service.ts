import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AcmeTransaction} from '../domain-defs';
import {AcmeMockHttpService} from './acme-mock-http.service';

@Injectable({
  providedIn: 'root'
})
export class AcmeTransactionService {
  private readonly mockData: AcmeTransaction[];

  constructor(private readonly mockHttp: AcmeMockHttpService) {
    this.mockData = [
      {accountId: 1, date: new Date('2009-01-18'), description: 'Misc Expenses', amount: -9.07},
      {accountId: 3, date: new Date('2020-12-02'), description: 'Birthday Money', amount: 10.00},
      {accountId: 1, date: new Date('2010-08-11'), description: 'Transfer to Account B', amount: -181.02},
      {accountId: 2, date: new Date('2010-08-11'), description: 'Transfer from Account A', amount: 181.02},
      {accountId: 1, date: new Date('2010-07-04'), description: 'Initial Deposit', amount: 100.00},
      {accountId: 1, date: new Date('2001-07-04'), description: 'Initial Deposit', amount: 1000.00},
      {accountId: 3, date: new Date('2020-12-10'), description: 'Bought Presents', amount: -143.76},
      {accountId: 2, date: new Date('2012-02-11'), description: 'Paycheck', amount: 2406.23},
      {accountId: 3, date: new Date('2020-11-02'), description: 'Initial Deposit', amount: 50.00},
      {accountId: 2, date: new Date('2020-10-31'), description: 'Account Closed', amount: -2592.25},
      {accountId: 1, date: new Date('2001-07-05'), description: 'Bought a goldfish', amount: -8.53},
      {accountId: 2, date: new Date('2010-08-10'), description: 'Initial Deposit', amount: 5.00},
      {accountId: 1, date: new Date('2002-12-23'), description: 'Holiday check from Grandma', amount: 12.15},
      {accountId: 1, date: new Date('2001-08-01'), description: 'School Fees', amount: -370.20},
      {accountId: 3, date: new Date('2020-12-11'), description: 'Overdraft Fee', amount: -23.00}
    ];
  }

  findByAccountId(id: number): Observable<AcmeTransaction[]> {
    return this.mockHttp.get(() => this.mockData.filter(t => t.accountId === id));
  }
}
