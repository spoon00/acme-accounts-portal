import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AcmeMockHttpService {
  constructor() {
  }

  get<T>(fn: () => T): Observable<T> {
    return new Observable<T>(s => {
      setTimeout(() => {
        s.next(fn());
        s.complete();
      }, this.mockHttpDelay());
    });
  }

  private mockHttpDelay(): number {
    return 50 + Math.floor(Math.random() * 1000);
  }
}
