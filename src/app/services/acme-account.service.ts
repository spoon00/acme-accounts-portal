import {Injectable} from '@angular/core';
import {AcmeAccount, AcmeSelectOption} from '../domain-defs';
import {Observable} from 'rxjs';
import {AcmeMockHttpService} from './acme-mock-http.service';

@Injectable({
  providedIn: 'root'
})
export class AcmeAccountService {
  private readonly mockData: AcmeAccount[];

  constructor(private readonly mockHttp: AcmeMockHttpService) {
    this.mockData = [
      {
        id: 1,
        clientId: 1,
        name: 'Account A',
        active: true,
        created: new Date('2001-07-04')
      },
      {
        id: 2,
        clientId: 1,
        name: 'Account B',
        active: false,
        created: new Date('2010-08-10')
      },
      {
        id: 3,
        clientId: 2,
        name: 'Account B',
        active: true,
        created: new Date('2020-11-02')
      }
    ];
  }

  findByAccountId(id: number): Observable<AcmeAccount> {
    return this.mockHttp.get(() => this.mockData.find(a => a.id === id));
  }

  findByClientId(id: number): Observable<AcmeAccount[]> {
    return this.mockHttp.get(() => this.mockData.filter(a => a.clientId === id));
  }
}
