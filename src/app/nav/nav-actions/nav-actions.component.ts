import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-nav-actions',
  templateUrl: './nav-actions.component.html',
  styleUrls: ['./nav-actions.component.css']
})
export class NavActionsComponent {
  @Input() accountId: number;
  @Input() clientId: number;
  @Input() viewEnabled: boolean;
  @Input() editEnabled: boolean;
  @Input() deleteEnabled: boolean;
  @Input() resetEnabled: boolean;

  @Output() readonly viewClicked: EventEmitter<void>;
  @Output() readonly editClicked: EventEmitter<void>;
  @Output() readonly deleteClicked: EventEmitter<void>;
  @Output() readonly resetClicked: EventEmitter<void>;

  constructor() {
    this.viewEnabled = false;
    this.editEnabled = false;
    this.deleteEnabled = false;
    this.resetEnabled = false;

    this.viewClicked = new EventEmitter<void>();
    this.editClicked = new EventEmitter<void>();
    this.deleteClicked = new EventEmitter<void>();
    this.resetClicked = new EventEmitter<void>();
  }
}
