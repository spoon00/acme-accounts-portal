import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AcmeSelectOption} from '../../domain-defs';

@Component({
  selector: 'app-nav-select',
  templateUrl: './nav-select.component.html',
  styleUrls: ['./nav-select.component.css']
})
export class NavSelectComponent implements OnInit {

  @Input() name: string;
  @Input() disabled: boolean;
  @Input() value: AcmeSelectOption;
  @Input() options: AcmeSelectOption[];

  @Output() readonly valueChange: EventEmitter<AcmeSelectOption>;

  constructor() {
    this.disabled = false;
    this.valueChange = new EventEmitter<AcmeSelectOption>();
  }

  ngOnInit(): void {
  }

}
