import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AcmeClientService} from '../services/acme-client.service';
import {AcmeSelectOption} from '../domain-defs';
import {AcmeAccountService} from '../services/acme-account.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  client: AcmeSelectOption;
  account: AcmeSelectOption;
  clients: AcmeSelectOption[] = [];
  accounts: AcmeSelectOption[] = [];

  constructor(private readonly router: Router,
              private readonly clientService: AcmeClientService,
              private readonly accountService: AcmeAccountService) {
  }

  ngOnInit(): void {
    this.reset();
  }

  viewAccount(): void {
    this.router.navigate([`/client/${this.client.id}/account/${this.account.id}`]);
  }

  clientSelected(client: AcmeSelectOption): void {
    this.resetAccount();
    if (client) {
      this.accountService.findByClientId(client.id)
        .subscribe(accounts => this.accounts = accounts.map(a => ({id: a.id, display: a.name})));
    }
  }

  reset(): void {
    this.resetClient();
    this.resetAccount();
    this.router.navigate(['']);
    this.clientService.getClientNames().subscribe(o => this.clients = o);
  }

  private resetAccount(): void {
    this.account = null;
    this.accounts = [];
  }

  private resetClient(): void {
    this.client = null;
    this.clients = [];
  }
}
