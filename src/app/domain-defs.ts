export interface AcmeClient {
  id: number;
  firstName: string;
  lastName: string;
  address: AcmeAddress;
}

export interface AcmeAddress {
  address1: string;
  address2: string;
  city: string;
  state_province: string;
  postal_code: string;
  country: string;
}

export interface AcmeAccount {
  id: number;
  clientId: number;
  name: string;
  created: Date;
  active: boolean;
}

export interface AcmeTransaction {
  accountId: number;
  date: Date;
  description: string;
  amount: number;
}

export interface AcmeSelectOption {
  id: number;
  display: string;
}
